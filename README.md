# Bootstrap Sass HTML Boilerplate
A quick starting point for a Bootstrap Sass project. Used by me with [Codekit 2](https://incident57.com/codekit/) as preprocessor but is theortically preprocessor agnostic. 

Based on:
- https://github.com/twbs/bootstrap-sass
- https://github.com/h5bp/html5-boilerplate

Benefits:
- Simple file structure, only one sass file to compile.
- Great for designers as a quick way to get up and running designing in the browser. 
- If you have Codekit 2, simply drag-and-drop into Codekit 2, and press preview.
- Codekit 2 handles sass preprocessor duties and spins up a local server/live reload.

## Non-Codekit 2 Setup 
Download the repo. The only sass file you need to compile is:

    scss/screen.scss

to...

    css/screen.css

NOTE: If you're using Codekit the `codekit.config` file will handle this for you. However, you can use any preproessor you like to work this wizardry.

## Customizing Bootstrap
Changes and customizations to Bootstrap should be made in the following order:

First, by manipulating Bootstraps variables in:

    bower_components/bootstrap-sass/assets/stylesheets/bootstrap/_variables.scss

Second, if the disired change cannot be made by using variables, use the provided Bootstrap Customizations sass file:

    scss/boostrap_cust.scss

Keep the Bootstrap Customizations ordered by the provided headings of Bootstrap elements.

## Custom CSS or Elements
Custom (non-Bootstrap related sass) sass elements should go in:

    scss/screen.scss

Keep all customizations below the imports at the top of the file.

## Creating HTML
An `index.html` file is provided. Add markup here. You can create additional pages as needed.

## Options for Codekit
Codekit provides basic support for partials by way of [.kit](https://incident57.com/codekit/help.html#kit) files. Please use the provided `partials` folder for these files.
